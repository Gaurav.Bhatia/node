var rect = require('./rectangle');

function solve(l, b) {
    if (l <= 0 || b <= 0) {
        console.log("Rectangle dimensions should be greater than 0");
    } else {
        console.log("The perimeter of rectangle is " + rect.perimeter(l, b));
        console.log("The area of rectangle is " + rect.area(l, b));
    }
}

solve(2, 4);
solve(6, 3);
solve(-3, 4);
solve(9, 0);